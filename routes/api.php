<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// CRUD opration in laravel API  


Route::post('/profile','profileController@create');
Route::get('/showdata','profileController@show');
Route::post('/update/{id}','profileController@update');
Route::post('/delete/{id}','profileController@delete');
