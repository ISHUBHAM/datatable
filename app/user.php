<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user extends Model
{
    function myPro()
    {
        return $this->hasOne('App\Profile','user_id','id'); 
    }
    function myPos()
    {
        return $this->hasMany('App\Post','user_id','id'); 
    }
}
