<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\user::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        
    ];
});

$factory->define(App\profile::class, function (Faker $faker) {
    return [

        'user_id' => rand(1,50),
        'name' => Str::random(10),
        'catagory'=>Str::random(10),
        'location'=>Str::random(10),
        
    ];
});


$factory->define(App\post::class, function (Faker $faker) {
    return [

        'user_id' => rand(1,50),
        'type' => Str::random(10),
        'catagory'=>Str::random(10),
        
    ];
});

